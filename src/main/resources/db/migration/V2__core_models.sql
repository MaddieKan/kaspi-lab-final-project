ALTER TABLE category
ADD COLUMN priority SMALLINT DEFAULT 1,
ADD COLUMN active BOOLEAN DEFAULT true;

CREATE TABLE publisher (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    location VARCHAR(200),
    website VARCHAR(100)
);

CREATE TABLE language (
    id SERIAL PRIMARY KEY,
    iso_code VARCHAR(5) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE genre (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE author (
    id BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    birth_date DATE,
    bio TEXT
);

CREATE TABLE author_language (
    author_id BIGINT REFERENCES author (id) ON DELETE CASCADE,
    language_id INTEGER REFERENCES language (id) ON DELETE CASCADE,
    PRIMARY KEY (author_id, language_id)
);

CREATE TABLE author_genre (
    author_id BIGINT REFERENCES author (id) ON DELETE CASCADE,
    genre_id INTEGER REFERENCES genre (id) ON DELETE CASCADE,
    PRIMARY KEY (author_id, genre_id)
);

CREATE TABLE book (
    id BIGSERIAL PRIMARY KEY,
    category INTEGER REFERENCES category (id) ON DELETE RESTRICT,
    name VARCHAR(200),
    publisher INTEGER REFERENCES publisher (id) ON DELETE RESTRICT,
    summary TEXT,
    language INTEGER REFERENCES language (id) ON DELETE RESTRICT,
    amount INTEGER NOT NULL DEFAULT 0,
    price DOUBLE PRECISION NOT NULL,
    discount SMALLINT DEFAULT 0,
    published DATE,
    pages SMALLINT,
    isbn VARCHAR(13)
);

CREATE TABLE author_book (
    author_id BIGINT REFERENCES author (id) ON DELETE CASCADE,
    book_id BIGINT REFERENCES book (id) ON DELETE CASCADE,
    PRIMARY KEY (author_id, book_id)
);

CREATE TABLE book_genre (
    book_id BIGINT REFERENCES book (id) ON DELETE CASCADE,
    genre_id INTEGER REFERENCES genre (id) ON DELETE CASCADE,
    PRIMARY KEY (book_id, genre_id)
);
