INSERT INTO role (name)
VALUES ('Admin'),
       ('Customer');

INSERT INTO main_user (username, password, email, phone, first_name, last_name)
VALUES ('MaddieKan', '$2a$10$6WCRo5.sP1EV1iclVJS6Au9mGM8dpEp5NAoVUFIk0Zia7o4rUMlBi',
        'dr.almanovamadina@gmail.com', '+77773544581', 'Madina', 'Almanova');

INSERT INTO user_role (user_id, role_id)
VALUES (1, 1);

INSERT INTO profile (user_id, city, address, birth_date)
VALUES (1, 'Balkhash', 'Kazbekova st. 55', '2000-11-05');
