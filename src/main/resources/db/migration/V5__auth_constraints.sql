ALTER TABLE main_user ADD CONSTRAINT unique_username UNIQUE (username);

ALTER TABLE main_user ADD CONSTRAINT unique_email UNIQUE (email);
