package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Genre;
import kz.kaspi.finalproject.core.services.GenreService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/genres")
public class AdminGenreController {
    private final GenreService genreService;

    @GetMapping("/add")
    public String genreForm(Model model) {
        model.addAttribute("genre", new Genre());
        model.addAttribute("action", "create");
        return "admin/genre/form";
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "genre") @Valid Genre genre,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Genre)
                    model.getAttribute("genre"), action);
        genreService.save(genre);
        return "redirect:/admin/core/genres";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("genres", genreService.list());
        return "admin/genre/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Integer id,
                           Model model) {
        Genre genre = genreService.retrieve(id);
        if (genre == null)
            return "admin/not-found";
        return addFormAttributes(model, genre, "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id,
                         Model model) {
        if (genreService.retrieve(id) == null)
            return "admin/not-found";
        genreService.delete(id);
        return "redirect:/admin/core/genres";
    }

    private String addFormAttributes(Model model,
                                     Genre genre,
                                     String action) {
        model.addAttribute("genre", genre);
        model.addAttribute("action", action);
        return "admin/genre/form";
    }
}
