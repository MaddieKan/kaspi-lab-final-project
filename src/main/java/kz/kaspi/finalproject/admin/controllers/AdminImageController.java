package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Book;
import kz.kaspi.finalproject.core.entities.Image;
import kz.kaspi.finalproject.core.services.BookService;
import kz.kaspi.finalproject.core.services.ImageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/images")
public class AdminImageController {
    private final ImageService imageService;
    private final BookService bookService;

    @GetMapping("/add")
    public String imageForm(Model model) {
        return addFormAttributes(model, new Image(),
                bookService.list(), "create");
    }

    @PostMapping("/create")
    public String save(@ModelAttribute(name = "image") Image image,
                       @RequestParam("file") MultipartFile multipartFile,
                       Model model) {
        imageService.save(image, multipartFile);
        return "redirect:/admin";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("images", imageService.list());
        return "admin/image/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Long id,
                           Model model) {
        Image image = imageService.retrieve(id);
        if (image == null)
            return "admin/not-found";
        return addFormAttributes(model, image, null, "update");
    }

    @PostMapping("/update")
    public String update(@ModelAttribute(name = "image") Image image,
                         Model model) {
        imageService.updatePriority(image);
        return "redirect:/admin/core/images";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id,
                         Model model) {
        if (!imageService.exists(id))
            return "admin/not-found";
        imageService.delete(id);
        return "redirect:/admin/core/images";
    }

    private String addFormAttributes(Model model,
                                     Image image,
                                     List<Book> books,
                                     String action) {
        model.addAttribute("image", image);
        model.addAttribute("books", books);
        model.addAttribute("action", action);
        return "admin/image/form";
    }
}
