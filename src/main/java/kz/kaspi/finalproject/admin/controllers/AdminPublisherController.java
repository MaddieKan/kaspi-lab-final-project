package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Publisher;
import kz.kaspi.finalproject.core.services.PublisherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/publishers")
public class AdminPublisherController {
    private final PublisherService publisherService;

    @GetMapping("/add")
    public String publisherForm(Model model) {
        return addFormAttributes(model, new Publisher(), "create");
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "publisher") @Valid Publisher publisher,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Publisher)
                    model.getAttribute("publisher"), action);
        publisherService.save(publisher);
        return "redirect:/admin/core/publishers";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("publishers", publisherService.list());
        return "admin/publisher/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Integer id,
                           Model model) {
        Publisher publisher = publisherService.retrieve(id);
        if (publisher == null)
            return "admin/not-found";
        return addFormAttributes(model, publisher, "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id,
                         Model model) {
        if (publisherService.retrieve(id) == null)
            return "admin/not-found";
        publisherService.delete(id);
        return "redirect:/admin/core/publishers";
    }

    private String addFormAttributes(Model model,
                                     Publisher publisher,
                                     String action) {
        model.addAttribute("publisher", publisher);
        model.addAttribute("action", action);
        return "admin/publisher/form";
    }
}
