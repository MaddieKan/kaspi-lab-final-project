package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Language;
import kz.kaspi.finalproject.core.services.LanguageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/languages")
public class AdminLanguageController {
    private final LanguageService languageService;

    @GetMapping("/add")
    public String languageForm(Model model) {
        return addFormAttributes(model, new Language(), "create");
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "language") @Valid Language language,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Language)
                    model.getAttribute("language"), action);
        languageService.save(language);
        return "redirect:/admin/core/languages";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("languages", languageService.list());
        return "admin/language/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Integer id,
                           Model model) {
        Language language = languageService.retrieve(id);
        if (language == null)
            return "admin/not-found";
        return addFormAttributes(model, languageService.retrieve(id),
                "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id,
                         Model model) {
        if (languageService.retrieve(id) == null)
            return "admin/not-found";
        languageService.delete(id);
        return "redirect:/admin/core/languages";
    }

    private String addFormAttributes(Model model,
                                     Language language,
                                     String action) {
        model.addAttribute("language", language);
        model.addAttribute("action", action);
        return "admin/language/form";
    }
}
