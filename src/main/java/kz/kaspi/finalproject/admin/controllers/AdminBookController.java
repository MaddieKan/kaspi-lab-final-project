package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.*;
import kz.kaspi.finalproject.core.services.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/books")
public class AdminBookController {
    private final BookService bookService;
    private final CategoryService categoryService;
    private final AuthorService authorService;
    private final PublisherService publisherService;
    private final LanguageService languageService;
    private final GenreService genreService;

    @GetMapping("/add")
    public String bookForm(Model model) {
        return addFormAttributes(model, new Book(), categoryService.list(),
                authorService.list(), publisherService.list(),
                languageService.list(), genreService.list(), "create");
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "book") @Valid Book book,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Book)
                    model.getAttribute("book"), categoryService.list(),
                    authorService.list(), publisherService.list(),
                    languageService.list(), genreService.list(), action);
        bookService.save(book);
        return "redirect:/admin/core/books";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("books", bookService.list());
        return "admin/book/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Long id,
                           Model model) {
        Book book = bookService.retrieve(id);
        if (book == null)
            return "admin/not-found";
        return addFormAttributes(model, book,
                categoryService.list(), authorService.list(),
                publisherService.list(), languageService.list(),
                genreService.list(), "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id,
                         Model model) {
        if (bookService.retrieve(id) == null)
            return "admin/not-found";
        bookService.delete(id);
        return "redirect:/admin/core/books";
    }

    private String addFormAttributes(Model model, Book book,
                                     List<Category> categories, List<Author> authors,
                                     List<Publisher> publishers, List<Language> languages,
                                     List<Genre> genres, String action) {
        model.addAttribute("book", book);
        model.addAttribute("categories", categories);
        model.addAttribute("authors", authors);
        model.addAttribute("publishers", publishers);
        model.addAttribute("languages", languages);
        model.addAttribute("genres", genres);
        model.addAttribute("action", action);
        return "admin/book/form";
    }
}
