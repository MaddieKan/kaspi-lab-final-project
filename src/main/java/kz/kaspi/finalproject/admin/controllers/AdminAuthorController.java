package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Author;
import kz.kaspi.finalproject.core.entities.Genre;
import kz.kaspi.finalproject.core.entities.Language;
import kz.kaspi.finalproject.core.services.AuthorService;
import kz.kaspi.finalproject.core.services.GenreService;
import kz.kaspi.finalproject.core.services.LanguageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/authors")
public class AdminAuthorController {
    private final AuthorService authorService;
    private final LanguageService languageService;
    private final GenreService genreService;

    @GetMapping("/add")
    public String authorForm(Model model) {
        return addFormAttributes(model, new Author(),
                languageService.list(), genreService.list(), "create");
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "author") @Valid Author author,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Author)
                    model.getAttribute("author"),
                    languageService.list(), genreService.list(), action);
        authorService.save(author);
        return "redirect:/admin/core/authors";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("authors", authorService.list());
        return "admin/author/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Long id,
                           Model model) {
        Author author = authorService.retrieve(id);
        if (author == null)
            return "admin/not-found";
        return addFormAttributes(model, authorService.retrieve(id),
                languageService.list(), genreService.list(), "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id,
                         Model model) {
        if (authorService.retrieve(id) == null)
            return "admin/not-found";
        authorService.delete(id);
        return "redirect:/admin/core/authors";
    }

    private String addFormAttributes(Model model, Author author,
                                     List<Language> languages,
                                     List<Genre> genres, String action) {
        model.addAttribute("author", author);
        model.addAttribute("languages", languages);
        model.addAttribute("genres", genres);
        model.addAttribute("action", action);
        return "admin/author/form";
    }
}
