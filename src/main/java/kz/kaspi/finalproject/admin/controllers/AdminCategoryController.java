package kz.kaspi.finalproject.admin.controllers;

import kz.kaspi.finalproject.core.entities.Category;
import kz.kaspi.finalproject.core.services.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/admin/core/categories")
public class AdminCategoryController {
    private final CategoryService categoryService;

    @GetMapping("/add")
    public String categoryForm(Model model) {
        return addFormAttributes(model, new Category(),
                categoryService.list(), "create");
    }

    @PostMapping("/save")
    public String save(@RequestParam String action,
                       @ModelAttribute(name = "category") @Valid Category category,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors())
            return addFormAttributes(model, (Category) model.getAttribute("category"),
                    categoryService.list(), action);
        categoryService.save(category);
        return "redirect:/admin/core/categories";
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("categories", categoryService.list());
        return "admin/category/list";
    }

    @GetMapping("/{id}")
    public String retrieve(@PathVariable("id") Integer id,
                           Model model) {
        Category category = categoryService.retrieve(id);
        if (category == null)
            return "admin/not-found";
        return addFormAttributes(model, category,
                categoryService.listWithoutOne(id), "update");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id,
                         Model model) {
        if (categoryService.retrieve(id) == null)
            return "admin/not-found";
        categoryService.delete(id);
        return "redirect:/admin/core/categories";
    }

    private String addFormAttributes(Model model, Category category,
                                 List<Category> categories, String action) {
        model.addAttribute("category", category);
        model.addAttribute("categories", categories);
        model.addAttribute("action", action);
        return "admin/category/form";
    }
}
