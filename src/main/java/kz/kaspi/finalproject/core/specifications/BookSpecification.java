package kz.kaspi.finalproject.core.specifications;

import kz.kaspi.finalproject.core.entities.Book;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class BookSpecification {

    static Specification<Book> categoryIdsIn(Set<Integer> ids) {
        return (book, query, builder) -> (book.get("category")).in(ids);
    }

    static Specification<Book> priceMin(Double price) {
        return (book, query, builder) ->
                builder.greaterThanOrEqualTo(book.get("price"), price);
    }

    static Specification<Book> priceMax(Double price) {
        return (book, query, builder) ->
                builder.lessThanOrEqualTo(book.get("price"), price);
    }

    static Specification<Book> discountMin(Short discount) {
        return (book, query, builder) ->
                builder.greaterThanOrEqualTo(book.get("discount"), discount);
    }

    static Specification<Book> discountMax(Short discount) {
        return (book, query, builder) ->
                builder.lessThanOrEqualTo(book.get("discount"), discount);
    }

    static Specification<Book> languageIdsIn(List<Integer> ids) {
        return (book, query, builder) -> (book.get("language")).in(ids);
    }

    static Specification<Book> languageIdsNotIn(List<Integer> ids) {
        return (book, query, builder) -> (book.get("language")).in(ids).not();
    }

    static Specification<Book> search(String text) {
        return (book, query, builder) -> builder.like(
                builder.lower(book.get("name")),
                "%" + text.toLowerCase() + "%");
    }

    static Specification<Book> notSold() {
        return (book, query, builder) -> builder.gt(book.get("amount"), 0);
    }

    static Specification<Book> notHidden() {
        return (book, query, builder) -> builder.isFalse(book.get("hidden"));
    }

    public static Specification<Book> buildSpecification(
            Set<Integer> categoryIds, List<Integer> languageIds,
            Map<String, String> filters) {

        return categoryIdsIn(categoryIds).and(notSold()).and(notHidden())
                .and(search(filters.getOrDefault("search", "")))
                .and(priceMin(NumberUtils.toDouble(
                        filters.get("priceMin"), 0)))
                .and(priceMax(NumberUtils.toDouble(
                        filters.get("priceMax"), 100000)))
                .and(discountMin(NumberUtils.toShort(
                        filters.get("discountMin"), (short) 0)))
                .and(discountMax(NumberUtils.toShort(
                        filters.get("discountMax"), (short) 100)))
                .and(languageIds == null
                        ? languageIdsNotIn(List.of(0)) : languageIdsIn(languageIds));
    }
}
