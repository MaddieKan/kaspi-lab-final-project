package kz.kaspi.finalproject.core.dto;

import kz.kaspi.finalproject.core.entities.Category;
import lombok.Data;

import java.util.List;

@Data
public class CategoryDto {
    private Integer id;
    private String name;
    private List<Category> parentCategories;
    private List<Category> subcategories;
}
