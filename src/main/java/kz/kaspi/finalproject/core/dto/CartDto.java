package kz.kaspi.finalproject.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartDto {
    private Double total;
    private Integer amount;
}
