package kz.kaspi.finalproject.core.dto;

import kz.kaspi.finalproject.utils.constants.OrderStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
public class OrderDto {
    private Long id;
    private LocalDateTime created;
    private String address;
    private OrderStatus status;
    private Double totalPrice;
    private Double deliveryPrice;
    private Set<OrderItemDto> orderItems;
}
