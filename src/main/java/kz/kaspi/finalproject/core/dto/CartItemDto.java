package kz.kaspi.finalproject.core.dto;

import kz.kaspi.finalproject.core.entities.Book;
import lombok.Data;

@Data
public class CartItemDto {
    private Long id;
    private Book book;
    private String bookImage;
    private Integer quantity;
    private Double sum;
}
