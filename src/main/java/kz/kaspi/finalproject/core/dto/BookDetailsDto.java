package kz.kaspi.finalproject.core.dto;

import kz.kaspi.finalproject.core.entities.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class BookDetailsDto {
    private Long id;
    private Category category;
    private String name;
    private Set<Image> images;
    private Set<Author> authors;
    private Publisher publisher;
    private String summary;
    private Language language;
    private Set<Genre> genres;
    private Integer amount;
    private Double price;
    private Double discountedPrice;
    private Short discount;
    private LocalDate published;
    private Short pages;
    private String ISBN;
}
