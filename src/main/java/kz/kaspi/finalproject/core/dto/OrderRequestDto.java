package kz.kaspi.finalproject.core.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OrderRequestDto {
    @NotBlank
    private String address;
}
