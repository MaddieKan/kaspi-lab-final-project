package kz.kaspi.finalproject.core.dto;

import kz.kaspi.finalproject.core.entities.Book;
import lombok.Data;

@Data
public class OrderItemDto {
    private Long id;
    private Book book;
    private String bookImage;
    private Integer quantity;
    private Double price;
    private Short discount;
    private Double sum;
}
