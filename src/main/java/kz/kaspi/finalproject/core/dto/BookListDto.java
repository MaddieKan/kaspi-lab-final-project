package kz.kaspi.finalproject.core.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BookListDto {
    private Long id;
    private String mainImage;
    private String name;
    private String authors;
    private String publisher;
    private LocalDate published;
    private Double price;
    private Double discountedPrice;
    private Integer amount;
    private Short discount;
    private Boolean inCart;
}
