package kz.kaspi.finalproject.core.controllers;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.core.dto.OrderRequestDto;
import kz.kaspi.finalproject.core.services.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;

    @PostMapping("/create")
    public String placeOrder(@AuthenticationPrincipal MainUser user,
                             @ModelAttribute(name = "order")
                             @Valid OrderRequestDto order,
                             BindingResult result,
                             Model model) {
        if (result.hasErrors())
            return "redirect:/cart";
        orderService.placeOrder(user, order);
        return "redirect:/books";
    }

    @GetMapping
    public String list(@AuthenticationPrincipal MainUser user,
                       Model model) {
        model.addAttribute("orders", orderService.list(user));
        return "core/order/list";
    }
}
