package kz.kaspi.finalproject.core.controllers;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.auth.services.MainUserService;
import kz.kaspi.finalproject.core.dto.CartItemDto;
import kz.kaspi.finalproject.core.dto.OrderRequestDto;
import kz.kaspi.finalproject.core.services.BookService;
import kz.kaspi.finalproject.core.services.CartItemService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/cart")
public class CartController {
    private final CartItemService cartItemService;
    private final MainUserService mainUserService;
    private final BookService bookService;

    @GetMapping("/add")
    public String addToCart(@RequestParam Long bookId,
                            @AuthenticationPrincipal MainUser user,
                            Model model) {
        if (!bookService.exists(bookId))
            return "core/not-found";
        if (cartItemService.exists(user.getId(), bookId))
            return "redirect:/books";
        cartItemService.addSingleBook(mainUserService
                .getReference(user.getUsername()),
                bookService.getReference(bookId));
        return "redirect:/books";
    }

    @GetMapping("/update/{id}")
    public String updateQuantity(@PathVariable Long id,
                                 @RequestParam Integer delta,
                                 @AuthenticationPrincipal MainUser user,
                                 Model model) {
        if (!cartItemService.existsById(user.getId(), id))
            return "core/not-found";
        cartItemService.save(id, delta);
        return "redirect:/cart";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id,
                         @AuthenticationPrincipal MainUser user,
                         Model model) {
        if (!cartItemService.existsById(user.getId(), id))
            return "core/not-found";
        cartItemService.delete(id);
        return "redirect:/cart";
    }

    @GetMapping
    public String list(@AuthenticationPrincipal MainUser user,
                       Model model) {
        List<CartItemDto> items = cartItemService.list(user.getId());
        model.addAttribute("cartItems", items);
        model.addAttribute("cart", cartItemService.getCart(items));
        model.addAttribute("order", new OrderRequestDto());
        return "core/cart/list";
    }
}
