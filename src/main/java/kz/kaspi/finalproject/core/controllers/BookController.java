package kz.kaspi.finalproject.core.controllers;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.core.services.BookService;
import kz.kaspi.finalproject.core.services.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/books")
@AllArgsConstructor
public class BookController {
    private final BookService bookService;
    private final CategoryService categoryService;

    @GetMapping
    public String booksInCategoryTree(@RequestParam(defaultValue = "1") Integer categoryId,
                                      @RequestParam(required = false) List<Integer> languageId,
                                      @RequestParam Map<String, String> filters,
                                      @AuthenticationPrincipal MainUser user,
                                      Model model) {
        if (!categoryService.exists(categoryId) ||
                filters.get("search") != null)
            categoryId = 1;
        model.addAttribute("category",
                categoryService.retrieveWithSubcategories(categoryId));
        model.addAttribute("books", bookService
                .listUnderCategory(categoryService
                        .listWithSubcategories(categoryId),
                        user != null ? user.getId() : null,
                        languageId, filters));
        return "core/book/list";
    }

    @GetMapping("/{id}")
    public String bookDetails(@PathVariable("id") Long id,
                              Model model) {
        if (!bookService.exists(id))
            return "core/not-found";
        model.addAttribute("book",
                bookService.retrieveBookDetails(id));
        return "core/book/details";
    }
}
