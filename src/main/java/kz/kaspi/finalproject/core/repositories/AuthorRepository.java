package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Author;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

    @EntityGraph(value = "Author.details")
    Optional<Author> findById(Long id);

    List<Author> findByHiddenFalse();
}
