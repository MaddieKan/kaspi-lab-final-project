package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.CartItem;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    @EntityGraph(value = "CartItem.details")
    List<CartItem> findByUserId(Long userId);

    @EntityGraph(value = "CartItem.details")
    List<CartItem> findByUserIdOrderById(Long userId);

    boolean existsByUserIdAndBookId(Long userId, Long bookId);

    boolean existsByUserIdAndId(Long userUd, Long id);

    void deleteByUserId(Long userId);
}
