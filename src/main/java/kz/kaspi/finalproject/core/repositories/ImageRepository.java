package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
}
