package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Book;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>,
        JpaSpecificationExecutor<Book> {

    @EntityGraph(value = "Book.details")
    Optional<Book> findById(Long id);

    @Override
    @EntityGraph(attributePaths = { "authors", "publisher" })
    List<Book> findAll();

    @EntityGraph(attributePaths = { "authors", "publisher", "images" })
    List<Book> findAll(Specification<Book> specification, Sort sort);

    @EntityGraph(attributePaths = { "authors", "publisher" })
    List<Book> findByHiddenFalse();
}
