package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Category;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @EntityGraph("Category.parent")
    Optional<Category> findById(Integer integer);

    List<Category> findByIdNotAndHiddenFalse(Integer id);

    List<Category> findByParentCategoryIdAndHiddenFalseOrderByPriorityAsc(Integer categoryId);

    @EntityGraph(attributePaths = {"subcategories"})
    Category findCategoryById(Integer id);

    List<Category> findByHiddenFalseOrderByPriorityAsc();
}
