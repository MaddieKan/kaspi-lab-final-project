package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {

    List<Publisher> findByHiddenFalse();
}
