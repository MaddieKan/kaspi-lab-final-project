package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Integer> {

    List<Language> findByHiddenFalse();
}
