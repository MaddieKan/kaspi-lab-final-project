package kz.kaspi.finalproject.core.repositories;

import kz.kaspi.finalproject.core.entities.OrderItem;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

    @EntityGraph(value = "OrderItem.details")
    Set<OrderItem> findByOrderId(Long orderId);
}
