package kz.kaspi.finalproject.core.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
public class Genre extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "genre_id_seq",
            sequenceName = "genre_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "genre_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name")
    @NotBlank(message = "{name.blank}")
    private String name;
}
