package kz.kaspi.finalproject.core.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@NamedEntityGraph(name = "Book.details",
        attributeNodes = { @NamedAttributeNode("authors"),
                @NamedAttributeNode("genres"),
                @NamedAttributeNode("category"),
                @NamedAttributeNode("publisher"),
                @NamedAttributeNode("language"),
                @NamedAttributeNode("images") })
public class Book extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "book_id_seq",
            sequenceName = "book_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "book_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "category")
    @NotNull(message = "{category.null}")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Category category;

    @Column(name = "name")
    @NotBlank(message = "{name.blank}")
    private String name;

    @ManyToMany
    @JoinTable(name = "author_book",
            joinColumns = { @JoinColumn(name = "book_id") },
            inverseJoinColumns = { @JoinColumn(name = "author_id") })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Author> authors;

    @ManyToOne
    @JoinColumn(name = "publisher")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Publisher publisher;

    @OneToMany(mappedBy = "book")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Image> images;

    @Column(name = "summary")
    @ToString.Exclude
    private String summary;

    @ManyToOne
    @JoinColumn(name = "language")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Language language;

    @ManyToMany
    @JoinTable(name = "book_genre",
            joinColumns = { @JoinColumn(name = "book_id") },
            inverseJoinColumns = { @JoinColumn(name = "genre_id") })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Genre> genres;

    @Column(name = "amount")
    @NotNull(message = "{amount.null}")
    private Integer amount;

    @Column(name = "price")
    @NotNull(message = "{price.null}")
    private Double price;

    @Column(name = "discount")
    @NotNull(message = "{discount.null}")
    private Short discount;

    @Column(name = "published")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate published;

    @Column(name = "pages")
    private Short pages;

    @Column(name = "isbn")
    private String ISBN;

    public Double getAdvantage() {
        return price * discount / 100;
    }

    public Double getDiscountedPrice() {
        return price - getAdvantage();
    }

    public String getMainImage() {
        if (images.isEmpty())
            return null;
        List<Image> temp = new ArrayList<>(images);
        temp.sort(Comparator.comparing(Image::getPriority));
        return temp.get(0).getImagePath();
    }
}
