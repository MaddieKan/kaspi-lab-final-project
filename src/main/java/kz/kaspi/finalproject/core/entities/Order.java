package kz.kaspi.finalproject.core.entities;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.utils.constants.OrderStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "shop_order")
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "shop_order_id_seq",
            sequenceName = "shop_order_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "shop_order_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private MainUser user;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "address")
    private String address;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.WAITING;

    @Column(name = "total_price")
    private Double totalPrice;

    @Column(name = "delivery_price")
    private Double deliveryPrice;

    @OneToMany(mappedBy = "order",
            cascade = { CascadeType.PERSIST })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<OrderItem> orderItems;

    public void calculateResult() {
        created = LocalDateTime.now();
        totalPrice = orderItems.stream()
                .mapToDouble(OrderItem::getSum).sum();
        deliveryPrice = 0.0;
    }
}
