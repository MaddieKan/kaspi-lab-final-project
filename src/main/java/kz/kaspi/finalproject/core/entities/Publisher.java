package kz.kaspi.finalproject.core.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
public class Publisher extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "publisher_id_seq",
            sequenceName = "publisher_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "publisher_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name")
    @NotBlank(message = "{name.blank}")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "website")
    private String website;
}
