package kz.kaspi.finalproject.core.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter @Setter
public abstract class BaseAbstractModel {
    @Column(name = "hidden")
    private Boolean hidden;

    @Column(name = "created")
    private LocalDateTime created;
}
