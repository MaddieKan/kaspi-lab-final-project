package kz.kaspi.finalproject.core.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
public class Image implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "image_id_seq",
            sequenceName = "image_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "image_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "path")
    private String path;

    @Column(name = "priority")
    private Short priority;

    public String getImagePath() {
        return "/images/books/" + id + "/" + path;
    }
}
