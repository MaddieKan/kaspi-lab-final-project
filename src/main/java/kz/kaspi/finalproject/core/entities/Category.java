package kz.kaspi.finalproject.core.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@NamedEntityGraph(name = "Category.parent",
        attributeNodes = { @NamedAttributeNode("parentCategory") })
public class Category extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "category_id_seq",
            sequenceName = "category_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "category_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name")
    @NotBlank(message = "{name.blank}")
    private String name;

    @Column(name = "priority")
    @Digits(integer = 4,
            fraction = 0,
            message = "{priority.short}")
    private Short priority;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Category parentCategory;

    @OneToMany(mappedBy = "parentCategory")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Category> subcategories;
}
