package kz.kaspi.finalproject.core.entities;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
public class Language extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "language_id_seq",
            sequenceName = "language_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "language_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "iso_code")
    @NotBlank(message = "{isocode.blank}")
    @Length(max = 5, message = "{isocode.length}")
    private String ISOCode;

    @Column(name = "name")
    @NotBlank(message = "{name.blank}")
    private String name;
}
