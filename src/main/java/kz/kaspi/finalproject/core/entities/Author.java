package kz.kaspi.finalproject.core.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@NamedEntityGraph(name = "Author.details",
        attributeNodes = { @NamedAttributeNode("languages"),
                @NamedAttributeNode("genres") })
public class Author extends BaseAbstractModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "author_id_seq",
            sequenceName = "author_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "author_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "first_name")
    @NotBlank(message = "{firstname.blank}")
    private String firstName;

    @Column(name = "last_name")
    @NotBlank(message = "{lastname.blank}")
    private String lastName;

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @Column(name = "bio")
    private String bio;

    @ManyToMany
    @JoinTable(name = "author_language",
            joinColumns = { @JoinColumn(name = "author_id") },
            inverseJoinColumns = { @JoinColumn(name = "language_id") })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Language> languages;

    @ManyToMany
    @JoinTable(name = "author_genre",
            joinColumns = { @JoinColumn(name = "author_id") },
            inverseJoinColumns = { @JoinColumn(name = "genre_id") })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Genre> genres;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
