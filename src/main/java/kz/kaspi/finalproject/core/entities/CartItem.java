package kz.kaspi.finalproject.core.entities;

import kz.kaspi.finalproject.auth.entities.MainUser;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@Table(name = "cart_item")
@NamedEntityGraph(name = "CartItem.details",
        attributeNodes = { @NamedAttributeNode("user"),
                @NamedAttributeNode(value = "book", subgraph = "Book.authors") },
        subgraphs = {
            @NamedSubgraph(name = "Book.authors",
                    attributeNodes = { @NamedAttributeNode("authors"),
                            @NamedAttributeNode("images") })
        })
public class CartItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "cart_item_id_seq",
            sequenceName = "cart_item_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "cart_item_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private MainUser user;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "quantity")
    private Integer quantity;

    public CartItem(MainUser user,
                    Book book,
                    Integer quantity) {
        this.user = user;
        this.book = book;
        this.quantity = quantity;
    }

    public Double getSum() {
        return book.getDiscountedPrice() * quantity;
    }
}
