package kz.kaspi.finalproject.core.entities;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "order_item")
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@NamedEntityGraph(name = "OrderItem.details",
        attributeNodes = { @NamedAttributeNode(value = "book",
                subgraph = "Book.authors") },
        subgraphs = {
                @NamedSubgraph(name = "Book.authors",
                        attributeNodes = { @NamedAttributeNode("authors"),
                                @NamedAttributeNode("images") })
        })
public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "order_item_id_seq",
            sequenceName = "order_item_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "order_item_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Order order;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price")
    private Double price;

    @Column(name = "discount")
    private Short discount;

    @Column(name = "total_price")
    private Double sum;
}
