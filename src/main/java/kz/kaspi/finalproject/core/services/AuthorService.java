package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.Author;
import kz.kaspi.finalproject.core.repositories.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService extends BaseAbstractService<Author, Long, AuthorRepository>{

    public AuthorService(AuthorRepository repository) {
        super(repository);
    }

    @Override
    public List<Author> list() {
        return super.getRepository().findByHiddenFalse();
    }
}
