package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.core.dto.CartDto;
import kz.kaspi.finalproject.core.dto.CartItemDto;
import kz.kaspi.finalproject.core.entities.Book;
import kz.kaspi.finalproject.core.entities.CartItem;
import kz.kaspi.finalproject.core.repositories.CartItemRepository;
import kz.kaspi.finalproject.utils.mappers.CartItemMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CartItemService {
    private final CartItemRepository repository;
    private final CartItemMapper cartItemMapper;

    public void addSingleBook(MainUser user, Book book) {
        repository.save(new CartItem(user, book, 1));
    }

    @Transactional
    public void save(Long id, Integer delta) {
        CartItem cartItem = repository.getById(id);
        if ((cartItem.getQuantity() + delta > 0 && delta < 0) ||
                (cartItem.getQuantity() + delta
                        <= cartItem.getBook().getAmount() && delta > 0))
            cartItem.setQuantity(cartItem.getQuantity() + delta);
    }

    public boolean exists(Long userId, Long bookId) {
        return repository.existsByUserIdAndBookId(userId, bookId);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public boolean existsById(Long userId, Long id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    public List<CartItemDto> list(Long userId) {
        return cartItemMapper.mapToCartItemDto(
                repository.findByUserIdOrderById(userId));
    }

    public CartDto getCart(List<CartItemDto> itemDtos) {
        itemDtos = itemDtos.stream().filter(item -> item.getBook().getAmount()
                >= item.getQuantity()).collect(Collectors.toList());

        return new CartDto(itemDtos.stream()
                .mapToDouble(CartItemDto::getSum).sum(), itemDtos.stream()
                        .mapToInt(CartItemDto::getQuantity).sum());
    }
}
