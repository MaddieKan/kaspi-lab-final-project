package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.BaseAbstractModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public abstract class BaseAbstractService<T extends BaseAbstractModel, ID extends Serializable,
        R extends JpaRepository<T, ID> > implements CrudService<T, ID> {

    private final R repository;

    @Override
    public T save(T entity) {
        entity.setHidden(false);
        entity.setCreated(LocalDateTime.now());
        return repository.save(entity);
    }

    @Override
    public boolean exists(ID id) {
        return repository.existsById(id);
    }

    @Override
    public T retrieve(ID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void delete(ID id) {
        T entity = repository.getById(id);
        entity.setHidden(true);
    }
}
