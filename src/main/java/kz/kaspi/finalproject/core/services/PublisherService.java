package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.Publisher;
import kz.kaspi.finalproject.core.repositories.PublisherRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherService extends BaseAbstractService<Publisher, Integer, PublisherRepository> {

    public PublisherService(PublisherRepository repository) {
        super(repository);
    }

    @Override
    public List<Publisher> list() {
        return super.getRepository().findByHiddenFalse();
    }
}
