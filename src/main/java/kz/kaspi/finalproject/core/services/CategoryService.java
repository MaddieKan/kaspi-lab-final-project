package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.dto.CategoryDto;
import kz.kaspi.finalproject.core.entities.Category;
import kz.kaspi.finalproject.core.repositories.CategoryRepository;
import kz.kaspi.finalproject.utils.mappers.CategoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CategoryService extends BaseAbstractService<Category, Integer, CategoryRepository>{
    private final CategoryMapper categoryMapper;

    public CategoryService(CategoryRepository repository,
                           CategoryMapper categoryMapper) {
        super(repository);
        this.categoryMapper = categoryMapper;
    }

    @Override
    public List<Category> list() {
        return super.getRepository()
                .findByHiddenFalseOrderByPriorityAsc();
    }

    public List<Category> listWithoutOne(Integer id) {
        return super.getRepository().findByIdNotAndHiddenFalse(id);
    }

    @Transactional
    public CategoryDto retrieveWithSubcategories(Integer id) {
        Category category = super.getRepository().findById(id).orElse(null);
        if (category != null)
            category.setSubcategories(super.getRepository()
                    .findByParentCategoryIdAndHiddenFalseOrderByPriorityAsc(id));
        return categoryMapper.mapToCategoryDto(category);
    }

    @Transactional
    public Set<Integer> listWithSubcategories(Integer id) {
        return listSubtreeVertices(super.getRepository()
                .findCategoryById(id),
                Stream.of(id).collect(Collectors.toSet()));
    }

    private Set<Integer> listSubtreeVertices(Category category,
                                             Set<Integer> vertices) {
        for (Category c : category.getSubcategories()) {
            if (!c.getHidden())
                vertices.add(c.getId());
            listSubtreeVertices(c, vertices);
        }
        return vertices;
    }
}
