package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.Image;
import kz.kaspi.finalproject.core.repositories.ImageRepository;
import kz.kaspi.finalproject.utils.FileUploadUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@AllArgsConstructor
public class ImageService {
    private final ImageRepository repository;

    public Image save(Image image, MultipartFile multipartFile) {
        String fileName = StringUtils
                .cleanPath(multipartFile.getOriginalFilename());
        image.setPath(fileName);
        Image savedImage = repository.save(image);
        String uploadDir = "images/books/" + savedImage.getId();
        try {
            FileUploadUtil.saveImage(uploadDir, fileName, multipartFile);
        } catch (IOException e) {
            System.err.println("Error occurred during image uploading");
        }
        return savedImage;
    }

    public List<Image> list() {
        return repository.findAll();
    }

    public boolean exists(Long id) {
        return repository.existsById(id);
    }

    public Image retrieve(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void updatePriority(Image image) {
        repository.getById(image.getId())
                .setPriority(image.getPriority());
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
