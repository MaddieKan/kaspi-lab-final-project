package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.BaseAbstractModel;

import java.io.Serializable;
import java.util.List;

public interface CrudService<T extends BaseAbstractModel, ID extends Serializable> {

    T save(T entity);

    List<T> list();

    boolean exists(ID id);

    T retrieve(ID id);

    void delete(ID id);
}
