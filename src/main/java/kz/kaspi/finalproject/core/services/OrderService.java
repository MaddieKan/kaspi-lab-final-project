package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.core.dto.OrderDto;
import kz.kaspi.finalproject.core.dto.OrderRequestDto;
import kz.kaspi.finalproject.core.entities.Order;
import kz.kaspi.finalproject.core.repositories.CartItemRepository;
import kz.kaspi.finalproject.core.repositories.OrderItemRepository;
import kz.kaspi.finalproject.core.repositories.OrderRepository;
import kz.kaspi.finalproject.utils.mappers.CartItemMapper;
import kz.kaspi.finalproject.utils.mappers.OrderItemMapper;
import kz.kaspi.finalproject.utils.mappers.OrderMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final CartItemRepository cartItemRepository;
    private final CartItemMapper cartItemMapper;
    private final OrderMapper orderMapper;
    private final OrderItemMapper orderItemMapper;

    @Transactional
    public void placeOrder(MainUser user, OrderRequestDto orderRequestDto) {
        Order order = orderMapper.mapToOrder(orderRequestDto);
        order.setUser(user);

        order.setOrderItems(cartItemMapper
                .mapToOrderItem(cartItemRepository
                        .findByUserId(user.getId()))
                .stream().filter(item -> item.getBook().getAmount()
                        >= item.getQuantity())
                .peek(item -> {item.setOrder(order);
                    item.getBook().setAmount(item.getBook()
                            .getAmount() - item.getQuantity());})
                .collect(Collectors.toSet()));

        order.calculateResult();
        orderRepository.save(order);
        cartItemRepository.deleteByUserId(user.getId());
    }

    public List<OrderDto> list(MainUser user) {
        List<OrderDto> orders = orderMapper
                .mapToOrderDto(orderRepository
                        .findByUserId(user.getId()));

        return orders.stream().peek(order -> order.setOrderItems(
                orderItemMapper.mapToOrderItemDto(orderItemRepository
                                .findByOrderId(order.getId()))))
                .collect(Collectors.toList());
    }
}
