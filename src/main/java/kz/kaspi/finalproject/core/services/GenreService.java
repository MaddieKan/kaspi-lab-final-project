package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.Genre;
import kz.kaspi.finalproject.core.repositories.GenreRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService extends BaseAbstractService<Genre, Integer, GenreRepository> {

    public GenreService(GenreRepository repository) {
        super(repository);
    }

    @Override
    public List<Genre> list() {
        return super.getRepository().findByHiddenFalse();
    }
}
