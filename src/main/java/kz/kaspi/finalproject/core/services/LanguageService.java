package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.entities.Language;
import kz.kaspi.finalproject.core.repositories.LanguageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService extends BaseAbstractService<Language, Integer, LanguageRepository> {

    public LanguageService(LanguageRepository repository) {
        super(repository);
    }

    @Override
    public List<Language> list() {
        return super.getRepository().findByHiddenFalse();
    }
}
