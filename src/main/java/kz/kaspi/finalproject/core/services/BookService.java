package kz.kaspi.finalproject.core.services;

import kz.kaspi.finalproject.core.dto.BookDetailsDto;
import kz.kaspi.finalproject.core.dto.BookListDto;
import kz.kaspi.finalproject.core.entities.Book;
import kz.kaspi.finalproject.core.repositories.BookRepository;
import kz.kaspi.finalproject.core.repositories.CartItemRepository;
import kz.kaspi.finalproject.core.specifications.BookSpecification;
import kz.kaspi.finalproject.utils.mappers.BookMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookService extends BaseAbstractService<Book, Long, BookRepository> {
    private final CartItemRepository cartItemRepository;
    private final BookMapper bookMapper;

    public BookService(BookRepository repository,
                       CartItemRepository cartItemRepository,
                       BookMapper bookMapper) {
        super(repository);
        this.cartItemRepository = cartItemRepository;
        this.bookMapper = bookMapper;
    }

    @Override
    public List<Book> list() {
        return super.getRepository().findByHiddenFalse();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Book entity = super.getRepository().getById(id);
        entity.setAmount(0);
        entity.setHidden(true);
    }

    public Book getReference(Long id) {
        return super.getRepository().getById(id);
    }

    public List<BookListDto> listUnderCategory(Set<Integer> categoryIds,
                                               Long userId,
                                               List<Integer> languageIds,
                                               Map<String, String> filters) {
        Sort sort;
        switch (filters.getOrDefault("sort", "")) {
            case "price":
                sort = Sort.by(Sort.Direction.ASC, "price");
                break;
            case "-price":
                sort = Sort.by(Sort.Direction.DESC, "price");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "id");
                break;
        }

        return super.getRepository().findAll(BookSpecification
                .buildSpecification(categoryIds, languageIds, filters), sort).stream()
                .map(bookMapper::mapToBookListDto).peek(item -> item.setInCart(
                        userId != null && cartItemRepository
                                .existsByUserIdAndBookId(userId, item.getId())))
                .collect(Collectors.toList());
    }

    public BookDetailsDto retrieveBookDetails(Long id) {
        return bookMapper.mapToBookDetailsDto(retrieve(id));
    }
}
