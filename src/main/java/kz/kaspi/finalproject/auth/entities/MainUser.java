package kz.kaspi.finalproject.auth.entities;

import kz.kaspi.finalproject.utils.validators.MatchFields;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "main_user")
@MatchFields.List({
        @MatchFields(
                field = "password",
                fieldMatch = "confirmedPassword",
                message = "{passwords.not.matching}"
        )
})
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode @ToString
@NamedEntityGraph(name = "MainUser.details",
        attributeNodes = { @NamedAttributeNode("roles") })
public class MainUser implements UserDetails, Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "main_user_id_seq",
            sequenceName = "main_user_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "main_user_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "username")
    @NotBlank(message = "{username.blank}")
    private String username;

    @Column(name = "password")
    @Length(min = 8, message = "{password.length}")
    private String password;

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private String confirmedPassword;

    @Column(name = "email")
    @Email(message = "{email.invalid}")
    @NotBlank(message = "{email.blank}")
    private String email;

    @Column(name = "phone")
    @NotBlank(message = "{phone.blank}")
    private String phone;

    @Column(name = "first_name")
    @NotBlank(message = "{firstname.blank}")
    private String firstName;

    @Column(name = "last_name")
    @NotBlank(message = "{lastname.blank}")
    private String lastName;

    @ManyToMany
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id") })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Role> roles;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }
}
