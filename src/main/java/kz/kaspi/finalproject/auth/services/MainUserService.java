package kz.kaspi.finalproject.auth.services;

import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.auth.repositories.MainUserRepository;
import kz.kaspi.finalproject.utils.UserValidationResult;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MainUserService implements UserDetailsService {

    private final MainUserRepository mainUserRepository;

    public MainUserService(MainUserRepository mainUserRepository) {
        this.mainUserRepository = mainUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MainUser user = mainUserRepository.findByUsername(username);
        if (user == null)
            throw new UsernameNotFoundException("{user.notfound}");
        return user;
    }

    public MainUser save(MainUser mainUser) {
        return mainUserRepository.save(mainUser);
    }

    public MainUser getReference(String username) {
        return mainUserRepository.getByUsername(username);
    }

    public UserValidationResult validate(MainUser mainUser) {
        UserValidationResult result = new UserValidationResult();
        if (mainUserRepository.findByUsername(mainUser.getUsername()) != null)
            result.setUsernameInvalid(true);
        if (mainUserRepository.findByEmail(mainUser.getEmail()) != null)
            result.setEmailInvalid(true);
        return result;
    }
}
