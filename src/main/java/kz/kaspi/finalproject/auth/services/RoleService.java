package kz.kaspi.finalproject.auth.services;

import kz.kaspi.finalproject.auth.entities.Role;
import kz.kaspi.finalproject.auth.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role retrieveByName(String name) {
        return roleRepository.findByName(name);
    }
}
