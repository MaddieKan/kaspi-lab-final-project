package kz.kaspi.finalproject.auth.controllers;


import kz.kaspi.finalproject.auth.entities.MainUser;
import kz.kaspi.finalproject.auth.services.MainUserService;
import kz.kaspi.finalproject.auth.services.RoleService;
import kz.kaspi.finalproject.utils.UserValidationResult;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Set;

@Controller
public class AuthController {
    private final MainUserService mainUserService;
    private final RoleService roleService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public AuthController(MainUserService mainUserService,
                          RoleService roleService,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.mainUserService = mainUserService;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping("/login")
    public String loginForm(Model model) {
        return "auth/login";
    }

    @GetMapping("/registration")
    public String registrationForm(Model model) {
        model.addAttribute("user", new MainUser());
        return "auth/registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute(name = "user") @Valid MainUser user,
                               BindingResult bindingResult,
                               Model model) {
        UserValidationResult result = mainUserService.validate(user);
        if (bindingResult.hasErrors() || result.hasErrors()) {
            model.addAttribute("user", model.getAttribute("user"));
            model.addAttribute("usernameExists", result.isUsernameInvalid());
            model.addAttribute("emailExists", result.isEmailInvalid());
            return "auth/registration";
        }
        user.setPassword(bCryptPasswordEncoder
                .encode(user.getPassword()));
        user.setConfirmedPassword(user.getPassword());
        user.setRoles(Set.of(
                roleService.retrieveByName("Customer")));
        mainUserService.save(user);
        return "auth/success";
    }

    @GetMapping("/forbidden")
    public String forbidden(Model model) {
        return "auth/forbidden";
    }
}
