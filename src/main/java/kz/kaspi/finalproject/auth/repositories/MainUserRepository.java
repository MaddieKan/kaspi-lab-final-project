package kz.kaspi.finalproject.auth.repositories;

import kz.kaspi.finalproject.auth.entities.MainUser;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainUserRepository extends JpaRepository<MainUser, Long> {

    @EntityGraph(value = "MainUser.details")
    MainUser findByUsername(String username);

    MainUser findByEmail(String email);

    MainUser getByUsername(String username);
}
