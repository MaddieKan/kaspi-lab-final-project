package kz.kaspi.finalproject.configurations;

import kz.kaspi.finalproject.auth.services.MainUserService;
import kz.kaspi.finalproject.utils.MainAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final MainUserService mainUserService;
    private final MainAccessDeniedHandler mainAccessDeniedHandler;

    public SecurityConfig(MainUserService mainUserService,
                          MainAccessDeniedHandler mainAccessDeniedHandler) {
        this.mainUserService = mainUserService;
        this.mainAccessDeniedHandler = mainAccessDeniedHandler;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf()
                    .disable()
                .authorizeRequests()
                    .antMatchers("/registration").not()
                        .fullyAuthenticated()
                    .antMatchers("/admin/**").hasRole("Admin")
                    .antMatchers("/cart/**").hasRole("Customer")
                    .antMatchers("/orders/**").hasRole("Customer")
                    .antMatchers("/images/**").permitAll()
                    .antMatchers("/books/**").permitAll()
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/books")
                    .permitAll()
                .and()
                    .logout()
                    .permitAll()
                    .logoutSuccessUrl("/books")
                .and()
                    .exceptionHandling()
                    .accessDeniedHandler(mainAccessDeniedHandler);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(mainUserService)
                .passwordEncoder(bCryptPasswordEncoder());
    }
}
