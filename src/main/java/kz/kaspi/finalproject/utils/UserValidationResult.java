package kz.kaspi.finalproject.utils;

public class UserValidationResult {
    private boolean usernameInvalid;
    private boolean emailInvalid;

    public UserValidationResult() {
        this.usernameInvalid = false;
        this.emailInvalid = false;
    }

    public boolean isUsernameInvalid() {
        return usernameInvalid;
    }

    public void setUsernameInvalid(boolean usernameInvalid) {
        this.usernameInvalid = usernameInvalid;
    }

    public boolean isEmailInvalid() {
        return emailInvalid;
    }

    public void setEmailInvalid(boolean emailInvalid) {
        this.emailInvalid = emailInvalid;
    }

    public boolean hasErrors() {
        return usernameInvalid || emailInvalid;
    }
}
