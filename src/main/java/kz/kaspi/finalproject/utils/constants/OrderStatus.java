package kz.kaspi.finalproject.utils.constants;

public enum OrderStatus {
    WAITING,
    CONFIRMED,
    PROCESSED,
    DELIVERED
}
