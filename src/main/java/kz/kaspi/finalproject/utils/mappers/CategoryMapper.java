package kz.kaspi.finalproject.utils.mappers;

import kz.kaspi.finalproject.core.dto.CategoryDto;
import kz.kaspi.finalproject.core.entities.Category;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class CategoryMapper {
    private final ModelMapper modelMapper;

    private final Converter<Category, List<Category> > parentsConverter =
        src -> {
            List<Category> categories = new ArrayList<>();
            Category category = src.getSource();
            while (category != null) {
                categories.add(category);
                category = category.getParentCategory();
            }
            Collections.reverse(categories);
            return categories;
        };

    public CategoryMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;

        modelMapper.createTypeMap(Category.class, CategoryDto.class)
                .addMappings(mapper -> mapper.using(parentsConverter)
                        .map(Category::getParentCategory,
                                CategoryDto::setParentCategories));
    }

    public CategoryDto mapToCategoryDto(Category category) {
        return modelMapper.map(category, CategoryDto.class);
    }
}
