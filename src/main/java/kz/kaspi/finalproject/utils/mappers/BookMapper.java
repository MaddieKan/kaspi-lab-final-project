package kz.kaspi.finalproject.utils.mappers;

import kz.kaspi.finalproject.core.dto.BookDetailsDto;
import kz.kaspi.finalproject.core.dto.BookListDto;
import kz.kaspi.finalproject.core.entities.Author;
import kz.kaspi.finalproject.core.entities.Book;
import kz.kaspi.finalproject.core.entities.Publisher;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookMapper {
    private final ModelMapper modelMapper;

    private final Converter<Set<Author>, String> authorsConverter =
            src -> StringUtils.join(src.getSource().stream()
                    .map(Author::getFullName)
                    .collect(Collectors.toList()), ", ");

    private final Converter<Publisher, String> publisherConvertor =
            src -> src.getSource().getName();

    public BookMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;

        modelMapper.createTypeMap(Book.class, BookListDto.class)
                .addMappings(mapper -> mapper.using(authorsConverter)
                        .map(Book::getAuthors, BookListDto::setAuthors))
                .addMappings(mapper -> mapper.using(publisherConvertor)
                        .map(Book::getPublisher, BookListDto::setPublisher))
                .addMapping(Book::getDiscountedPrice, BookListDto::setDiscountedPrice)
                .addMapping(Book::getMainImage, BookListDto::setMainImage);

        modelMapper.createTypeMap(Book.class, BookDetailsDto.class)
                .addMapping(Book::getDiscountedPrice,
                        BookDetailsDto::setDiscountedPrice);
    }

    public BookListDto mapToBookListDto(Book book) {
        return modelMapper.map(book, BookListDto.class);
    }

    public List<BookListDto> mapToBookListDto(List<Book> books) {
        return books.stream().map(this::mapToBookListDto)
                .collect(Collectors.toList());
    }

    public BookDetailsDto mapToBookDetailsDto(Book book) {
        return modelMapper.map(book, BookDetailsDto.class);
    }
}
