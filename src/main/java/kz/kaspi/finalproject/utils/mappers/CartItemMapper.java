package kz.kaspi.finalproject.utils.mappers;

import kz.kaspi.finalproject.core.dto.CartItemDto;
import kz.kaspi.finalproject.core.entities.CartItem;
import kz.kaspi.finalproject.core.entities.OrderItem;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CartItemMapper {
    private final ModelMapper modelMapper;

    public CartItemMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;

        modelMapper.createTypeMap(CartItem.class, CartItemDto.class)
                .addMapping(item -> item.getBook().getMainImage(),
                        CartItemDto::setBookImage);

        modelMapper.createTypeMap(CartItem.class, OrderItem.class)
                .addMappings(new PropertyMap<>() {
                    @Override
                    protected void configure() {
                        skip(destination.getId());
                    }
                })
                .addMapping(item -> item.getBook().getPrice(),
                        OrderItem::setPrice)
                .addMapping(item -> item.getBook().getDiscount(),
                        OrderItem::setDiscount);
    }

    public CartItemDto mapToCartItemDto(CartItem cartItem) {
        return modelMapper.map(cartItem, CartItemDto.class);
    }

    public List<CartItemDto> mapToCartItemDto(List<CartItem> cartItems) {
        return cartItems.stream().map(this::mapToCartItemDto)
                .collect(Collectors.toList());
    }

    public OrderItem mapToOrderItem(CartItem cartItem) {
        return modelMapper.map(cartItem, OrderItem.class);
    }

    public List<OrderItem> mapToOrderItem(List<CartItem> cartItems) {
        return cartItems.stream().map(this::mapToOrderItem)
                .collect(Collectors.toList());
    }
}
