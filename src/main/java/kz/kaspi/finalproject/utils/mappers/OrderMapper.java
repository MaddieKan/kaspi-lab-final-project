package kz.kaspi.finalproject.utils.mappers;

import kz.kaspi.finalproject.core.dto.OrderDto;
import kz.kaspi.finalproject.core.dto.OrderRequestDto;
import kz.kaspi.finalproject.core.entities.Order;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper {
    private final ModelMapper modelMapper;

    public OrderMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;

        modelMapper.createTypeMap(OrderRequestDto.class, Order.class);

        modelMapper.createTypeMap(Order.class, OrderDto.class)
            .addMappings(new PropertyMap<>() {
                @Override
                protected void configure() {
                    skip(destination.getOrderItems());
                }
            });
    }

    public Order mapToOrder(OrderRequestDto orderRequestDto) {
        return modelMapper.map(orderRequestDto, Order.class);
    }

    public OrderDto mapToOrderDto(Order order) {
        return modelMapper.map(order, OrderDto.class);
    }

    public List<OrderDto> mapToOrderDto(List<Order> orders) {
        return orders.stream().map(this::mapToOrderDto)
                .collect(Collectors.toList());
    }
}
