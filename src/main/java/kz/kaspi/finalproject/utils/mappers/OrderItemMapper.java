package kz.kaspi.finalproject.utils.mappers;

import kz.kaspi.finalproject.core.dto.OrderItemDto;
import kz.kaspi.finalproject.core.entities.OrderItem;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class OrderItemMapper {
    private final ModelMapper modelMapper;

    public OrderItemMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;

        modelMapper.createTypeMap(OrderItem.class, OrderItemDto.class)
                .addMapping(item -> item.getBook().getMainImage(),
                        OrderItemDto::setBookImage);
    }

    public OrderItemDto mapToOrderItemDto(OrderItem orderItem) {
        return modelMapper.map(orderItem, OrderItemDto.class);
    }

    public Set<OrderItemDto> mapToOrderItemDto(Set<OrderItem> orderItems) {
        return orderItems.stream().map(this::mapToOrderItemDto)
                .collect(Collectors.toSet());
    }
}
